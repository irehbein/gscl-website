---
layout: page
title: Honorary Members
thumbnail: "assets/img/ehrenmitglieder.jpg"
img: "assets/img/ehrenmitglieder.jpg"
tags: [resources]
lang: en
---

Die Gesellschaft für Sprachtechnologie und Computerlinguistik e.V. (GSCL) wurde 1975 unter dem Namen "LDV-Fittings e.V.", später als "Gesellschaft für Linguistische Datenverarbeitung (GLDV) e. V zur Förderung der wissenschaftlichen linguistischen Datenverarbeitung gegründet. Seit September 2008 trägt sie den jetzigen Namen.


## Prof. Dr. Burghard Rieger

On 20.09.2012 in Vienna, the German Society for Computational Linguistics and Language Technology decided to appoint Prof. Dr. Burghard Rieger as honorary member. The honours ceremony took place on 27.09.2013 at the GSCL Conference in Darmstadt.

![]({{ "/assets/img/rieger.jpg" | relative_url }})

## Prof. Dr. Winfried Lenders †
On 01.10.2009 in Potsdam, the German Society for Computational Linguistics and Language Technology decided to appoint Prof. Dr. Winfried Lenders for its first honorary member. With the founding of the LDV-Fittings in 1975, Mr Lenders established the pathbreaking precursor of the GSCL. From 1993 to 1997, he chaired the then renamed Association for Computational Linguistics (GLDV), and was a long-standing member of the Advisory Board. His ongoing commitment contributed significantly to the profile of our scientific society.

Prof. Dr. Winfried Lenders passed away on May 1, 2015. With him, the GSCL loses not only a highly respected scientist, but also a longtime dedicated supporter of the association's work. We will deeply miss him.

![]({{ "/assets/img/lenders-1408x.jpg" | relative_url }})