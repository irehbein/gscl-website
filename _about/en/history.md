---
layout: page
title: Short History of the GSCL
thumbnail: "assets/img/library.jpg"
img: "assets/img/library.jpg"
tags: [resources]
lang: en
---

The German Society for Computational Linguistics and Language Technology (GSCL) was founded in 1975 as "LDV-Fittings e.V." - a society for the promotion of scientific linguistic data processing. Later, until 2008, the society was called "Gesellschaft für Linguistische Datenverarbeitung (GLDV)".


## Presidents

* 1975 bis 1976 - Prof. Dr. Hans G. Tillmann
* 1976 bis 1981 - Prof. Dr. Dieter Krallmann
* 1981 bis 1985 - Prof. Dr. Jürgen Krause
* 1985 bis 1987 - Prof. Dr. Peter Hellwig
* 1987 bis 1989 - Prof. Dr. Brigitte Endres-Niggemeyer
* 1989 bis 1991 - Prof. Dr. Burghard Rieger
* 1991 bis 1993 - Prof. Dr. Ursula Klenk
* 1993 bis 1997 - Prof. Dr. Winfried Lenders
* 1997 bis 2001 - Prof. Dr. Roland Hausser
* 2001 bis 2007 - Prof. Dr. Henning Lobin
* 2007 bis 2009 - Prof. Dr. Angelika Storrer
* 2009 bis 2015 - Prof. Dr. Manfred Stede
* 2015 bis 2017 - Prof. Dr. Heike Zinsmeister
* 2017 bis heute - Prof. Dr. Torsten Zesch
