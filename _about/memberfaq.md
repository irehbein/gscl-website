---
layout: page
title: GSCL Member FAQ
thumbnail: "assets/img/portfolio/faq-300x225.jpg"
img: "assets/img/portfolio/faq-300x225.jpg"
tags: [resources]
lang: de
---

Diese Seite soll viele der Fragen behandeln, die neue Mitglieder haben können. Wenn Sie das Gefühl haben, dass etwas fehlt oder eine bessere Erklärung benötigt wird, kontaktieren Sie uns bitte (vorsitzende@gscl.org).

## Wie kann ich mit anderen Mitgliedern in Kontakt treten und Leute mit ähnlichen Interessen treffen?
Die wichtigsten jährlichen Networking-Events, die von der GSCL mitgetragen werden, sind die KONVENS (für alle) und die TaCoS (für Studenten). Diese finden normalerweise in einer deutschen, schweizerischen oder österreichischen Stadt statt und dauern etwa 3 Tage. Wenn Sie daran interessiert sind, Menschen mit ähnlichen Interessen zu treffen, sollten Sie in Erwägung ziehen, einer Interessengruppe beizutreten oder eine zu eröffnen. Diese Gruppen sind dynamisch und können je nach Interessen der Mitglieder mehrere Jahre oder kürzer bestehen. Neben den monatlichen GSCL Research and Tutorial Talks veranstalten wir unregelmäßig (derzeit virtuell) Workshops zu bestimmten Themen oder Meetups.

## Wie läuft die Kommunikation innerhalb der GSCL ab? Wo kann ich Fragen stellen, etc.?
Der Hauptkommunikationskanal für Mitglieder ist unsere Mailingliste, in die Sie automatisch aufgenommen werden, wenn Sie der GSCL beitreten. Die Posts enthalten Informationen der GSCL-Komitees, Neuigkeiten, Einladungen zu Workshops, Calls for Papers, Stellenausschreibungen usw. Stellen Sie Ihre Fragen auch gerne in diese Mailingliste. Unsere LinkedIn-Community wächst, sodass dies hoffentlich irgendwann auch ein etwas informellerer Kommunikationskanal sein wird. Bitte denken Sie daran, auch dort aktiv zu werden, stellen Sie Ihre Fragen etc. Die Links zu unserer LinkedIn-Gruppe und unserem Twitter-Account finden Sie auf der Startseite.

## Wie kann ich Mitglied der GSCL werden?
Falls Sie neu eintreten möchten, beachten Sie bitte folgendes: Jede Neumitgliedschaft muss zuerst beantragt werden. Dazu gibt es ein Formular auf unserer Webseite, das vollständig ausgefüllt werden muss. Ihre Mitgliedschaft wird dann – falls es keine Einsprüche des Vorstands gibt – vom Informationsreferenten / von der Informationsreferentin üblicherweise innerhalb von 14 Tagen bestätigt. Mit Ihrem bestätigten Eintritt werden Sie Vereinsmitglied beim GSCL e.V. und im Regelfall auch einmal jährlich beitragspflichtig – es sei denn, Sie sind immatrikulierter Studierender (dies gilt unabhängig vom Studiengang und auch für Promovierende).
Für Ihre Mitgliedschaft und für deren Ende gilt die Vereinssatzung in ihrer gültigen Form. Die Satzung ist ebenfalls auf der GSCL Webseite verfügbar.

## Was passiert mit meinen Daten?
Die von Ihnen angegebene E-Mail-Adresse wird in den GSCL-Verteiler eingetragen und gilt als Ansprech-Adresse auch für die/den Schatzmeister*in. Die von Ihnen angegebenen weiteren Daten wie die postalische Adresse werden ebenfalls für die Dauer Ihrer Mitgliedschaft (regulär plus 1 Jahr nach Ende) für die Zusendung des jährlichen postalischen GSCL Newsletter und für die Rechnungsstellung des Mitgliedsbeitrags verwendet. Halten Sie uns daher bitte immer über Ihre aktuelle postalische und elektronische Adresse auf dem Laufenden (bitte per email an schatzmeister@gscl.org).

## Wie stelle ich sicher, dass meine Studierendenmitgliedschaft kostenfrei ist?
Für Studierende gilt: Solange Sie mindestens einmal jährlich Ihre Immatrikulations- bzw. Studienbescheinigung einreichen (an schatzmeister@gscl.org), bleiben Sie beitragsfrei, in dem Jahr, in dem keine solche Bescheinigung mehr bei uns vorliegt, sind Sie verpflichtet, den fälligen Mitgliedsbeitrag (derzeit 50 Euro) jeweils spätestens im Sommer (Juli – September) zu überweisen. Sie können auch dem/der Schatzmeister*in natürlich auch ein SEPA-Lastschriftmandat erteilen – das spart uns einigen administrativen Aufwand. Um höheren administrativen Aufwand des ehrenamtlich geleisteten Schatzmeisteramts zu vermeiden, bitten wir Sie dringend, diese Bescheinigung ohne, dass vorher eine Aufforderung erfolgen muss, jeweils vor Juli des Beitagsjahres einzureichen.

## Wie kann ich meine Mitgliedschaft kündigen?
Falls Sie Ihre Mitgliedschaft beenden möchten, beachten Sie bitte folgendes: Ein Ende der Mitgliedschaft ist jeweils nur zum 31.12. eines Jahres möglich, die Kündigung muss jeweils vor Ende September desselben Jahres erfolgen. Ein Wiedereintritt ist jederzeit möglich, allerdings ist dabei zu beachten, dass noch offene Beiträge auch nach Ende der Mitgliedschaft (nach §195 BGB) für drei weitere Jahre fällig und innerhalb dieser Frist zum Wiedereintrittsdatum beglichen sein müssen.
