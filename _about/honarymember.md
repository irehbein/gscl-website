---
layout: page
title: Ehrenmitglieder
thumbnail: "assets/img/ehrenmitglieder.jpg"
img: "assets/img/ehrenmitglieder.jpg"
tags: [resources]
lang: de
---

Die Gesellschaft für Sprachtechnologie und Computerlinguistik e.V. (GSCL) wurde 1975 unter dem Namen "LDV-Fittings e.V.", später als "Gesellschaft für Linguistische Datenverarbeitung (GLDV) e. V zur Förderung der wissenschaftlichen linguistischen Datenverarbeitung gegründet. Seit September 2008 trägt sie den jetzigen Namen.


## Prof. Dr. Burghard Rieger

Am 20.09.2012 beschloss die Deutsche Gesellschaft für Computerlinguistik und Sprachtechnologie in Wien, Prof. Dr. Burghard Rieger zum Ehrenmitglied zu ernennen. Die Preisverleihung fand am 27.09.2013 auf der GSCL-Konferenz in Darmstadt statt.

![]({{ "/assets/img/rieger.jpg" | relative_url }})

## Prof. Dr. Winfried Lenders †
Am 01.10.2009 beschloss die Deutsche Gesellschaft für Computerlinguistik und Sprachtechnologie in Potsdam, Prof. Dr. Winfried Lenders zum ersten Ehrenmitglied zu ernennen. Mit der Gründung der LDV-Fittings im Jahr 1975 etablierte Prof. Lenders den wegweisenden Vorläufer der GSCL. Von 1993 bis 1997 war er Vorsitzender der damals umbenannten Vereinigung für Computerlinguistik (GLDV) und langjähriges Mitglied des Beirats. Sein kontinuierliches Engagement trug wesentlich zum Profil unserer wissenschaftlichen Gesellschaft bei.

Prof. Dr. Winfried Lenders verstarb am 1. Mai 2015. Mit ihm verliert die GSCL nicht nur einen hoch angesehenen Wissenschaftler, sondern auch einen langjährigen engagierten Unterstützer der Arbeit des Vereins. Wir werden ihn sehr vermissen.

![]({{ "/assets/img/lenders-1408x.jpg" | relative_url }})