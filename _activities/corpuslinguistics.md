---
layout: page
title: German Sentiment Analysis
feature-img: "assets/img/sigs/books.jpg"
permalink: /activities/germansentiment/
tags: [SIG]
lang: de
---
Vorsitzende: [Melanie Siegel](melanie.siegel@h-da.de), [Michael Wiegand](michael.wiegand@aau.at)
Website: [sites.google.com/site/iggsahome](https://sites.google.com/site/iggsahome)

Der Arbeitskreis Stimmungsanalyse, der auch unter dem Namen "Interest Group for German Sentiment Analysis" (IGGSA) bekannt ist, stellt eine Interessengruppe für Stimmungsanalyse im deutschsprachigen Raum sowohl für Forschung als auch Industrie dar.

Die Stimmungsanalyse beschäftigt sich mit der Extraktion und Klassifikation von meinungstragenden Ausdrücken in (geschriebener und gesprochener) Sprache. Typische Aufgaben sind die Unterscheidung von Meinungen und neutralem Text, die Klassifikation von Meinungen nach ihrer Polarität oder sogar Emotion, sowie die Extraktion der beteiligten Entitäten (d.h. Meinungsträger und Meinungsziel). Gerade im Hinblick auf die sozialen Medien spielt die Stimmungsanalyse eine wichtige Rolle. Die Fülle an meinungstragender Information, die sie beinhalten, stellen einerseits eine wertvolle Informationsquelle für diverse Interessengruppen (z.B. Analysten oder Marketingstrategen) dar; anderseits ist ein gezielter Informationszugriff nur durch computergestützte Anwendungen möglich, die das Knowhow aus der Stimmungsanalyse erfordern.

Das Hauptinteresse dieses Arbeitskreises gilt vor allem der Bereitstellung von Ressourcen (d.h. Lexika und Korpora) für die Verarbeitung deutscher Sprache. Das MLSA-Korpus, ein Referenzkorpus, das Sentimentinformation auf unterschiedlichen Textebenen beinhaltet, ist bereits aus Arbeiten von Mitgliedern der Interessengruppe hervorgegangen.
