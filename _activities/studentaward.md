---
layout: page
title: GSCL-Preis für studentische Abschlussarbeiten
lang: de
feature-img: "assets/img/award.jpg"
tags: [awards]
---

Alle zwei Jahre vergibt die GSCL zwei mit jeweils 400 Euro dotierte
Preise für die beste studentische Bachelor-Abschlussarbeit sowie für die
beste Master-Arbeit auf dem Gebiet der Sprachtechnologie und
Computerlinguistik.

Die Nominierung erfolgt durch eine/n Betreuer/in bzw. Gutachter/in.
Aufgerufen sind daher die Dozentinnen und Dozenten an Universitäten und
Fachhochschulen, ihre besten Absolventinnen und Absolventen der
vergangenen zwei Jahre zur Einreichung einer Zusammenfassung ihrer
Abschlussarbeit zu ermuntern. Auf Grundlage eines Begutachtungsprozesses
(\"blind reviewing\") wählen die Mitglieder des GSCL-Vorstands in einer
Vorauswahl die besten Arbeiten aus. Diese werden dann von den
Autor/innen während der **Konferenz zur Verarbeitung natürlicher Sprache
(KONVENS)** im Rahmen einer Endausscheidung präsentiert. Reisekosten und
Tagungsgebühren trägt die GSCL.

Die **aktuelle Auswahlrunde** erfolgt im Sommer 2021 mit der
Endausscheidung bei der KONVENS-Tagung in Düsseldorf (6.-9. September
2021). Es können Arbeiten eingereicht werden, deren Abgabedatum im
Zeitraum April 2019 bis März 2021 liegt. Die Nominierungen müssen bis
zum 15.6.2021 per E-Mail an <gscl-preis@gscl.org> erfolgen.

Für die Nominierung sind folgende Unterlagen im PDF-Format einzureichen:

1.  Zusammenfassung der Arbeit (deutsch oder englisch) im Umfang von
    maximal fünf Seiten (einspaltig, eineinhalbzeilig, 11pt) zzgl.
    Referenzen und Abbildungen. Die Autorenschaft darf aus der
    Zusammenfassung nicht hervorgehen, auch Zitate sind ggf. zu
    anonymisieren.
2.  Beiblatt: Name und E-Mailadresse von Autor/in und Betreuer/in,
    Hochschule, erzielter Abschlussgrad. Die Benotung soll nicht
    angegeben werden.
3.  Kurze Stellungnahme/Empfehlungsschreiben durch Betreuer/in oder
    Gutachter/in.

Teilnahmeberechtigt sind alle BA/MA-Arbeiten aus deutschsprachigen
Ländern. Studierende an Universitäten in anderen Ländern dürfen sich
bewerben, sofern sich ihre Arbeit mit der deutschen Sprache als
Untersuchungsgegenstand befasst.

![](https://gscl.org/media/pages/auszeichnungen/1970975524-1615117512/students-702090_960_720.jpg)

\

## PreisträgerInnen seit 2001

2021
-   Master: Marie Bexte (Duisburg): Combined Analysis of Image and Text
    Using Visio-Linguistic Neural Models - A Case Study on Robustness
    Within an Educational Scoring Task
-   Bachelor: Yannic Bracke (Potsdam): Automatic text classification
    with imbalanced data - Building a frame classifier from a corpus of
    editorials

2019
-   Master: Isabel Meraner (Zürich): Grasping the Nettle: Neural Entity
    Recognition for Scientific and Vernacular Plant Names
-   Bachelor: Rami Aly (Hamburg): Hierarchical writing genre
    classification with neural networks

2017
-   Master: Mathias Müller (Zürich): Treatment of Markup in Statistical
    Machine Translation
-   Bachelor: Katarina Ragna Krüger (Potsdam): Assessing the Dimensions
    of Factuality in Biomedical Text

2015
-   Master: Edo Collins (Tübingen): Classifying German Noun-Noun
    Compounds Using Stacked Denoising Autoencoders
-   Bachelor: Glorianna Jagfeld (Stuttgart): Towards a Better Semantic
    Role Labeling of Complex Predicates

2013
-   Marcel Bollmann (Bochum): Automatic Normalization for Linguistic
    Annotation of Historical Language Data

2011
-   Christian M. Meyer (Darmstadt): Combining Answers from Heterogenous
    Web Documents for Question Answering

2009
-   Christian Hardmeier (Basel): Using Linguistic Annotations in
    Statistical Machine Translation of Film Subtitles
-   Pierre Lison (Saarbrücken): Robust Processing of Spoken Dialogue

2007
-   Jette Klein-Berning (Heidelberg): Multilingual Information Retrieval
    with Latent Semantic Indexing

2005
-   Hans-Friedrich Witschel (Leipzig): Text, Words, Morphes:
    Possibilities of Automatic Terminology Extraction

2003
-   David Reitter (Potsdam): Rhetorical Analysis with Rich-Feature
    Support Vector Models

2001
-   Georg Rehm (Osnabrück): Preliminary Considerations for the Automatic
    Summary of German Texts Using an SGML and DSSSL-Based Representation
    of RST Relations
