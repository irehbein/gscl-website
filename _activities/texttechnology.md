---
layout: page
title: Texttechnologie
feature-img: "assets/img/sigs/code.jpg"
permalink: /activities/texttechnology/
tags: [SIG]
lang: de
---
Leitung: [Gertrud Faaß](faassg@uni-hildesheim.de), [Roman Schneider](schneider@ids-mannheim.de)

Der AK Texttechnologie befasst sich vorrangig mit der Integration von Standards der Textstrukturierung (Markup Languages) und der linguistischen Datenverarbeitung. Ziel ist es, dadurch die Entwicklung innovativer Textmodelle und inhaltsorientierter Textverarbeitung- und nutzung zu ermöglichen. Das Hauptaugenmerk richtet sich auf die Verarbeitung deutschsprachiger Texte und innovativer Textsorten (z.B. [Songtexte](http://songkorpus.de/)); der Anspruch bezieht sich auch auf Sprachvarietäten, Sozio- und Regiolekte und die kontrastive Untersuchung bisher wenig erforschter Sprachen, was ggf. eine Überprüfung und Erweiterung existierender Standards erfordert.

Für die Erkennung von Unterschieden zwischen diesen Sprachen und Sprachvarianten und bereits gut untersuchten Sprachen, d.h. für kontrastive Forschung, ist vordererst die Kompilation passender Datenbestände (Korpora) nötig. Hier kümmert sich der Arbeitskreis u.a. um die Definition relevanter Metadatenkategorien und unterstützt die Erstellung und Dokumentation einschlägiger Goldstandards.

# Call for Papers

[Challenges in Computational Linguistics, Empiric Research & Multidisciplinary Potential of German Song Lyrics](https://songkorpus.de/CfP_Songlyrics_JLCL_de.pdf)
