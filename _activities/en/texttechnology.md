---
layout: page
title: Texttechnologie
feature-img: "assets/img/sigs/code.jpg"
permalink: /activities/texttechnology/
tags: [SIG]
lang: en
---
Chair: [Gertrud Faaß](faassg@uni-hildesheim.de), [Roman Schneider](schneider@ids-mannheim.de)

SIG Text Technology is concerned with the integration of markup languages and linguistic data processing. The goal is to enable the development of innovative text models and content-oriented word processing and usage. Our main focus is on the processing of German-language texts and innovative text types (e.g. [song lyrics[(http://songkorpus.de/)]); the claim also relates to language varieties, socio- and regiolects and the contrastive examination of less-researched languages, which may require a review and extension of existing standards.

For the detection of differences between these languages or language variants and already well-studied languages, i.e. for contrastive research, the compilation of suitable data (corpora) seems necessary. Here the SIG takes care of the definition of relevant metadata categories, and supports the creation and documentation of relevant gold standards.

# Call for Papers

[Challenges in Computational Linguistics, Empiric Research & Multidisciplinary Potential of German Song Lyrics](https://songkorpus.de/CfP_Songlyrics_JLCL_de.pdf)
