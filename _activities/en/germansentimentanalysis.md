---
layout: page
title: German Sentiment Analysis
feature-img: "assets/img/sigs/laptop.jpg"
permalink: /activities/germansentiment/
tags: [SIG]
lang: en
---
Chair: [Melanie Siegel](melanie.siegel@h-da.de), [Michael Wiegand](michael.wiegand@aau.at)
Website: [sites.google.com/site/iggsahome](https://sites.google.com/site/iggsahome)

# Mission Statement
The area of sentiment analysis draws more and more attention not only in the academic field but also in the business area (e.g. web- and business intelligence). In general, sentiment analysis refers to the task of identifying and extracting opinions, emotions and appraisals from a given input stream (e.g. text documents, product reviews, micro-blogging services or speech) with respect to a certain target. While Sentiment Analysis is a highly active research area in the English (and international) community, there are rarely research collaborations and resources available that focus on the German language as the target domain.

This project is a European research collaboration, which addresses German Sentiment Analysis. The collaboration involves currently eight partners from three different countries within Europe. Emphasized issues ranging from theory to applications of computer-aided Opinion Mining, Sentiment Analysis, Polarity Detection and Affective Computing. While the members of this project assume various viewpoints of sentiment analysis, they ultimately all work within a common research area, which makes this endeavour an exciting and interesting collaboration.

Inter-communication and resources are the keywords within this research group, but also services which may be offered amongst interested parties and academic-related communities. Our short-term goal is to work intensively on available resources (dictionaries, lexicons and corpora), and improve their quality and acceptability by the research community. The main target language is German, and obviously the German community, since the number of German benchmark collections, corpora, subjectivity dictionaries or other resources is rather limited.

A first milestone of this research collaboration will be the creation (and the establishment) of a first (Golden Standard) benchmark collection for the German language, in order to allow a coherent and comparable evaluation of Sentiment Analysis algorithms and systems.
