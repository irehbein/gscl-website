---
layout: page
title: Ethics Crash Course
feature-img: "assets/img/portfolio/ethics-crash-course-screenshot-300x225.png"
img: "assets/img/portfolio/ethics-crash-course-screenshot-300x225.png"
permalink: /resources/ethics-crash-course
date: June 27, 2021
tags: [Ethics]
lang: en
---
This page contains a set of ready-made teaching materials for a crash course in ethics for natural language processing. The materials are published under CC-BY and intended to be integrated into introductory NLP courses.

The Google slides can be found [here](https://docs.google.com/presentation/d/1wixbRzZHnDdPiR2JACFDPITo3PTRDaMF0NibhiPxCEQ/edit?usp=sharing)..

We recommend accessing the Google slides for obtaining the most recent version of our slides.
However, in case you cannot access Google slides, we also provide [pptx](https://gscl.org/media/pages/resources/ethics-crash-course/1693884166-1621235377/crash_course_on_ethics_in_nlp_v1.0.pptx) and [pdf](https://gscl.org/media/pages/resources/ethics-crash-course/1330253914-1621235371/crash_course_on_ethics_in_nlp_v1.0.pdf) versions for download-

If you have any suggestions, feel free to add your comments directly to the Google slides, but also please e-mail us.

The intention behind the teaching materials is described in the following paper:
Annemarie Friedrich and Torsten Zesch: A Crash Course on Ethics for Natural Language Processing. In Proceedings of the Fifth Workshop on Teaching NLP. June 2021. ([poster](https://gscl.org/media/pages/resources/ethics-crash-course/3577554522-1621235368/teachingnlp_ethics_poster.pdf))

[GermEval Questionaire](../germeval)
