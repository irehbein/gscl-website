---
layout: page
title: Corpus Linguistics
feature-img: "assets/img/sigs/books.jpg"
permalink: /activities/corpuslinguistics/
tags: [SIG]
lang: en
---
Chair: Alexander Mehler , Armin Hoenen

The working group Corpus Linguistics and Quantitative Linguistics deals with the development and testing of tools for the automatic analysis of corpora as well as the construction and application of mathematical, quantitative models of explorative corpus analysis.

The working group addresses the following questions:
* Preparation and annotation of corpora.
* Body analytic based metrization of properties and relations of linguistic units.
* Extraction, reconstruction or exploration of linguistic knowledge from corpora of natural language texts.
* Promotion of applications in the field of text analysis and text technology.
* Support of linguistic theories.
