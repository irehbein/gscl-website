---
layout: page
title: Computerlinguistik für die Politik- und Sozialwissenschaften
feature-img: "assets/img/sigs/politics.jpg"
permalink: /activities/politicssocialsciences/
tags: [SIG]
lang: en
---
Chair: [Goran Glavaš](mailto:goran@informatik.uni-mannheim.de), [Gabrielle Lapesa](mailto:gabriella.lapesa@ims.uni-stuttgart.de), [Simone Ponzetto](mailto:simone@informatik.uni-mannheim.de), [Ines Rehbein](ines@informatik.uni-mannheim.de)

The newly founded Special Interest Group wants to provide a forum for researchers at the interface between computational linguistics/NLP and the text-as-data community from political and social science, so as to foster collaboration and further interdisciplinary research work between the communities.

Possible initiatives for the new SIG include, but are not limited to:
* Creation of resources and tools for political text analysis
* Creation of German benchmarking datasets for tasks in the political sciences
* Organisation of workshops for CL for Social and Political Sciences
* Organisation of shared tasks on topics relevant for political text analysis
* Bringing German CL and "text-as-data PolSci" communities closer together

Of course we are also open for further initiatives and activities and are looking forward to your ideas!

Picture: CC-BY-4.0: © European Union 2019 – Source: EP, Creator: Gabor KOVACS

* [CPSS@KONVENS 2022](/{{site.baseurl}}/2022/01/01/cpss2022.html)
* [CPSS@KONVENS 2021](/{{site.baseurl}}/2021/01/01/cpss2021.html)
