---
layout: page
title: GSCL-Promotionspreis zum Gedenken an Wolfgang Hoeppner
lang: de
feature-img: "assets/img/award.jpg"
tags: [awards]
---

Die GSCL vergibt alle zwei Jahre einen Preis für eine herausragende
Dissertation im Bereich der Sprachtechnologie bzw. Computerlinguistik.
Die Dissertation muss in einem Themengebiet des Faches bzw. seiner
Teilgebiete angesiedelt sein.

**Ausschreibung 2022**

Bewerber sollen ihre Dissertation bereits verteidigt haben, allerdings darf die Verteidigung nicht vor Mai 2020 stattgefunden haben. Die Arbeit darf bereits für andere Preise nominiert worden sein. Teilnahmeberechtigt sind alle Dissertationen aus deutschsprachigen Ländern (Deutschland, Österreich und die Schweiz) oder aus anderen Ländern, sofern sie sich mit der deutschen Sprache als Untersuchungsgegenstand befassen.

Der Arbeit müssen folgende Unterlagen (im PDF-Dateiformat) beigefügt
sein:

-   die Dissertationsschrift
-   mindestens ein Empfehlungsschreiben (in der Regel von der/dem
    Erstgutachter/In)
-   Zusammenfassung der Dissertation (max. 10 Seiten)
-   eine Publikationsliste der Doktorandin bzw. des Doktoranden
-   ein Curriculum Vitae

Preiswürdig sind Arbeiten, die einen großen Fortschritt für die
Sprachtechnologie und Computerlinguistik bedeuten und das Fach
signifikant voranbringen.

Der Preis ist mit 1000,- Euro dotiert. Einsendeschluss für den Promotionspreis 2022 ist der 31. Mai 2022, 23:59 Uhr MESZ. Alle Dokumente sind fristgerecht in deutscher oder englischer Sprache per E-Mail an den Sprecher des wissenschaftlichen Beirats der GSCL ([schneider@ids-mannheim.de]) zu senden. Die Preisverleihung soll im Rahmen der KONVENS 2022 stattfinden. Die Anzahl der Preisträger kann gegebenenfalls erweitert und der Preis geteilt werden. Der Rechtsweg ist ausgeschlossen.

![](https://gscl.org/media/pages/auszeichnungen/5589017-1615118697/phd.jpg)

## PreisträgerInnen seit 2014
2018
-   Annemarie Friedrich (Universität des Saarlandes): [States, events,
    and generics: computational modeling of situation entity
    types](https://publikationen.sulb.uni-saarland.de/bitstream/20.500.11880/23722/1/scidok_final.pdf)
-   Referees: Manfred Pinkal, Alexis Palmer

2016
-   Martin Riedl (Technische Universität Darmstadt): [Unsupervised
    Methods for Learning and Using Semantics of Natural
    Language](https://tuprints.ulb.tu-darmstadt.de/5435/7/Dissertation_Riedl_Unsupervised_Methods_Semantics.pdf)
-   Referees: Chris Biemann, Anders Søgaard

2014
-   Nadir Durrani (Stuttgart): [A Joint Translation Model with
    Integrated Reordering](http://dx.doi.org/10.18419/opus-2987)
-   Honorable Mention: Annelen Brunner (Würzburg): Automatische
    Erkennung von Redewiedergabe in literarischen Texten
