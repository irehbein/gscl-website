---
layout: page
title: Social Media / Internetbasierte Kommunikation
feature-img: "assets/img/sigs/apps-blur-button.jpg"
img: "assets/img/sigs/apps-blur-button.jpg"
permalink: /activities/socialmedia/
tags: [SIG]
lang: de
---
Vorsitzende: [Michael Beiswenger](michael.beisswenger@tu-dortmund.de), [Torsten Zesch](mailto:torsten.zesch@uni-due.de)

Der Arbeitskreis beschäftigt sich mit den linguistischen, sprachtechnologischen und texttechnologischen Grundlagen, die für den Aufbau annotierter Korpora zur Sprachverwendung in sozialen Medien und in der internetbasierten Kommunikation sowie entsprechender Datenanteile in Webkorpora benötigt werden. Zur internetbasierten Kommunikation (engl. auch "computer-mediated communication") werden dabei dialogische Kommunikationsformen gerechnet, die das Internet als Kommunikations-Infrastruktur nutzen - beispielsweise die Kommunikation in Online-Foren, Chats, Instant-Messaging-Anwendungen und via Skype, auf Wiki-Diskussionsseiten, in Kommentar-Threads von Weblogs und Videoplattformen, auf Twitter und auf den Profilseiten sozialer Netzwerke sowie in multimodalen Interaktionsräumen (Lernumgebungen, MMORPGs und "virtuellen Welten").

Zu den Themenfeldern des Arbeitskreises gibt es national und international bereits Initiativen (u.a. im Rahmen der Text Encoding Initiative). An diese schließt der AK an, um gemeinsam mit ForscherInnen aus Linguistik, Computerlinguistik und Sprachtechnologie Lösungen speziell für deutschsprachige Daten zu erarbeiten.

# Thematische Schwerpunkte

Der Arbeitskreis verstetigt Themen, Projekte und Diskussionslinien mit computerlinguistischem, sprach- und texttechnologischem Bezug, die im Rahmen des DFG-Netzwerks [Empirische Erforschung internetbasierter Kommunikation](http://www.empirikom.net/) (Empirikom) behandelt wurden und die für die Entwicklung von Verfahren für die Verarbeitung und Annotation von Sprachdaten aus sozialen Medien und aus Genres internetbasierter Kommunikation von zentraler Bedeutung sind. Dazu gehören:
* die Verankerung des Themas "Social Media / Internetbasierte Kommunikation" auf der Agenda nationaler und internationaler Standardisierungsinitiativen im Bereich der Sprach- und Texttechnologie;
* die Dokumentation von Annotationsrichtlinien, Goldstandards und Ergebnissen aus Projekten zur Anpassung existierender NLP-Verfahren für die automatische linguistische Annotation von Sprachdaten aus sozialen Medien und aus Genres internetbasierter Kommunikation;
* die Erstellung standardisierter Komponenten für die automatische Verarbeitung von Sprachdaten aus sozialen Medien und aus Genres internetbasierter Kommunikation, u.a. in Kooperation mit den Entwicklerteams von Apache UIMA und des DKPro Frameworks; es ist geplant, die Komponenten im UIMA-Standard zu entwickeln und als Teil von DKPro frei verfügbar zu machen;
* die Dokumentation von Rechtefragen in Bezug auf die Erhebung, Annotation und Bereitstellung von Sprachdaten aus den behandelten Genres in Korpora und ihrer Nutzung für Zwecke der empirischen Sprachanalyse und im Bereich der Sprachtechnologie;
* der Aufbau eines Netzwerks von ForscherInnen, die sich im In- und Ausland mit den im AK bearbeiteten Fragestellungen beschäftigen (auf der Grundlage existierender Kontakte und Kooperationen).

# Aktivitäten
Geplant sind regelmäßige Workshops zu wechselnden Schwerpunktthemen, der Austausch über eine Mailingliste und einen digitalen Newsletter sowie eine Dokumentation aktueller Projekte und Veranstaltungen mit Bezug zu den Themen des AK auf der GSCL-Website.

* Workshop des AK im Rahmen der KONVENS 2014: "NLP 4 CMC: Natural Language Processing for
  * Computer-Mediated Communication / Social Media"
  * Universität Hildesheim, 6. Oktober 2014
  * [Website zum Workshop und Call for papers](https://sites.google.com/site/nlp4cmc)
* Workshop "Social Media Corpora for the eHumanities: Standards, Challenges, and Perspectives"
  * TU Dortmund, 20./21. Februar 2014
  Programm: [empirikom.net/pub/Aktivitaeten/WebHome/empirikom-7.pdf](http://empirikom.net/pub/Aktivitaeten/WebHome/empirikom-7.pdf)
  Im Zentrum des Workshops stehen Themen, die in den vergangenen dreieinhalb Jahren Arbeitsschwerpunkte des DFG-Netzwerks "Empirische Erforschung internetbasierter Kommunikation" bildeten: Am Beispiel von Korpusprojekten aus Deutschland, Frankreich, den Niederlanden, Italien und der Schweiz werden Fragen der linguistischen Beschreibung der Sprachverwendung in sozialen Medien sowie korpus- und computerlinguistische Aspekte des Aufbaus, der Annotation und der Verarbeitung von Korpora zur Sprache im Internet und in sozialen Medien thematisiert.

# Vernetzung und Kooperationen
* Der AK nutzt bestehende Kontakte und Kooperationen aus dem DFG-Netzwerk [Empirische Erforschung internetbasierter Kommunikation](http://www.empirikom.net/) sowie zum Entwicklerteam von Apache UIMA und des DKPro Frameworks.
* Für den Bereich der Erarbeitung und Standardisierung von Repräsentationsschemata ist eine enge Zusammenarbeit mit der Special Interest Group [Computer-Mediated Communication](http://www.tei-c.org/Activities/SIG/CMC/) im Rahmen der [Text Encoding Initiative](http://www.tei-c.org/) (TEI) geplant.
* Für den Bereich der Anpassung von Tagsets für deutsche Sprachdaten an die Besonderheiten der behandelten Genres kooperiert der AK mit der Arbeitsgruppe zur Überarbeitung des Stuttgart-Tübingen-Tagset (STTS).
* Existierende Kontakte zu vegleichbaren Netzwerken in anderen europäischen Ländern (z.B. zur französischen Special Interest Group [Nouvelles formes de communication](https://groupes.renater.fr/wiki/corpus-ecrits-nouvcom/index) (Nouv-com)) und zu Projekten aus dem Netzwerk [Building and Annotating Corpora of Computer-Mediated Communication](https://wiki.itmc.tu-dortmund.de/cmc/) werden im Rahmen des AK weiter ausgebaut. Geplant sind u.a. gemeinsame Workshops, in denen Fragen der Verarbeitung und Annotation von Daten aus sozialen Medien und aus Genres internetbasierter Kommunikation für verschiedene Sprachen diskutiert werden.