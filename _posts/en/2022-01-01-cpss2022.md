---
layout: post
title: CPSS@Konvens 2022
permalink: /arbeitskreise/cpss/cpss-2022/
feature-img: "assets/img/workshop2022.jpg"
thumbnail: "assets/img/workshop2022.jpg"
tags: [conferences]
lang: en
---

# 2nd Workshop on Computational Linguistics for Political Text Analysis


Co-located with KONVENS 2022 in Potsdam Germany, Sep 12, 2022 
go to last year's workshop: CPSS 2021

Keynote talk

We are very happy that [David Jurgens](https://jurgens.people.si.umich.edu/) has accepted to give a keynote at CPSS 2022!

# Panel Speakers

tba

# Workshop description

Recent years have seen more and more applications of computational methods to the analysis of political texts. This has led to the emergence of different communities (one with a background in political / social science and the other in the areas of NLP/computer science) which struggle to get awareness of the relevant work taking place in the respective other community.

The main goal of our workshop is to bridge this gap and bring together researchers and ideas from the different communities, to foster collaboration and catalyze further interdisciplinary research efforts.

To this end, our workshop will accept two types of workshop submissions:

archival papers describing original and unpublished work
non-archival papers (abstracts) that present already published research or ongoing work.
The availability of both submission formats will meet the different needs of researchers from different communities, allowing them to come together and exchange ideas in an "get to know each other" environment which is likely to foster future collaborations.

In addition, the workshop will host a panel with invited speakers from the different communities, with a focus on theory-driven modelling of complex political or socio-psychological constructs in text, such as populism, polarisation or political cynicism. The panel will discuss challenges for modelling such multifaceted concepts from a theoretical as well as from a machine learning point of view, and how such models can be evaluated in a meaningful way.

We hope that this will foster discussions and allow us to reflect on our different research practices, methods and tools, and will help to improve the communication between our fields.

# Organisers

Ines Rehbein, Christopher Klamm, Simone Ponzetto (U-Mannheim)
Gabriella Lapesa (U-Stuttgart)

# Call for papers

go to [CfP](https://gscl.org/en/arbeitskreise/cpss/cpss-2022/cfp-2022)

# Workshop Programme

tba

# Workshop Proceedings

will be published here in Sep 2022

# Programme committee

[go to PC](https://gscl.org/en/arbeitskreise/cpss/cpss-2022/programme-committee)

