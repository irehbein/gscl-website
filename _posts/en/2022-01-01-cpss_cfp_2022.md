---
layout: post
title: CPSS@Konvens 2022 - Call for Papers
permalink: /arbeitskreise/cpss/cpss-2022/cfp-2022/
feature-img: "assets/img/workshop2022.jpg"
thumbnail: "assets/img/workshop2022.jpg"
tags: [conferences]
lang: en
---

# 2nd Workshop on Computational Linguistics for Political Text Analysis


Co-located with KONVENS 2022 in Potsdam Germany, Sep 12, 2022 
go to last year's workshop: CPSS 2021

Keynote talk

We are very happy that [David Jurgens](https://jurgens.people.si.umich.edu/) has accepted to give a keynote at CPSS 2022!

# Panel Speakers

tba

# Workshop description

Recent years have seen more and more applications of computational methods to the analysis of political texts. This has led to the emergence of different communities (one with a background in political / social science and the other in the areas of NLP/computer science) which struggle to get awareness of the relevant work taking place in the respective other community.

The main goal of our workshop is to bridge this gap and bring together researchers and ideas from the different communities, to foster collaboration and catalyze further interdisciplinary research efforts.

## Keynote Talk
We are very happy that David Jurgens has accepted to give a keynote at CPSS 2022!

# Important Dates
| Workshop Papers due | 10.07.2022 |
| Notification of acceptance | 20.08.2022 |
| Camera-ready papers due | 01.09.2022 |
| Workshop date | 12.09.2022 |

# Submissions

We welcome submissions of long and short papers, posters, and demonstrations relating to any aspect of Political Text Analysis using computational methods. Our workshop will accept two types of submissions:

archival papers describing original and unpublished work
non-archival papers (abstracts) that present already published research or ongoing work.
The availability of both submission formats will meet the different needs of researchers from different communities, allowing them to come together and exchange ideas in an "get to know each other" environment which is likely to foster future collaborations.

Submissions should report original and unpublished research on topics of interest to the workshop. Accepted papers are expected to be presented at the workshop. Archival papers will be published in the workshop proceedings. They should emphasize obtained results rather than intended work, and should indicate clearly the state of completion of the reported results.

An archival paper accepted for presentation at the workshop must not be or have been presented at any other meeting with publicly available proceedings.

Submission is electronic, using the EASYCHAIR conference management system, which can be found at this address:
[easychair.org/conferences](https://easychair.org/conferences/?conf=cpss2022)

Long/short archival paper submissions must use the official KONVENS 2022 style templates [download link](https://konvens2022.uni-potsdam.de/wp-content/uploads/2022/02/acl2020-templates.zip). Non-archival papers (abstracts) should use this style template and must be submitted as pdf. Long papers must not exceed eight (8) pages of content. Short papers and demonstration papers must not exceed four (4) pages of content. Non-archival abstracts must not exceed one (1) page. References do not count against these limits.

Note: Supplementary material does not count towards page limit and should be included in the appendix.

# Reviewing
Reviewing of papers will be double-blind. Therefore, the paper must not include the authors' names and affiliations or self-references that reveal the authors’ identity – e.g., "We previously showed (Smith, 1991) ..." should be replaced with citations such as "Smith (1991) previously showed ...". Papers that do not conform to these requirements will be rejected without review.

Dual Submission Policy: Dual submissions of papers are allowed. Authors of archival papers that have been or will be submitted to other meetings or publications must provide this information to the workshop co-chairs (rehbein@uni-mannheim.de). In your message, please list the names and dates of the conferences, workshops or meetings where you have submitted or plan to submit your paper in addition to CPSS. Authors of accepted papers must notify the program chairs within 10 days of acceptance if the paper is withdrawn for any reason.

# Special Theme and Panel
In addition, the workshop will host a panel with invited speakers from the different communities, with a focus on theory-driven modelling of complex political or socio-psychological constructs in text, such as populism, polarisation or political cynicism. The panel will discuss challenges for modelling such multifaceted concepts from a theoretical as well as from a machine learning point of view, and how such models can be evaluated in a meaningful way.

# Other Questions
For more information, please refer to the workshop website:

gscl.org/en/arbeitskreise/cpss/cpss-2022

If you have any questions, please feel free to contact the program co-chairs: rehbein@uni-mannheim.de

# Organisers

Ines Rehbein, Christopher Klamm, Simone Ponzetto (U-Mannheim)
Gabriella Lapesa (U-Stuttgart)
