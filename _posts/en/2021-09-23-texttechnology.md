---
layout: post
title: SIG Text Technology
tags: [SIG]
lang: en
---

The GSCL Special Interest Group "Text Technology" invites paper contributions for the 2022 JLCL special issue "Challenges in Computational Linguistics, Empiric Research & Multidisciplinary Potential of German Song Lyrics". [Call for Papers](https://songkorpus.de/CfP_Songlyrics_JLCL_de.pdf)