---
layout: post
title: Member Meeting 2020
tags: [membermeeting]
lang: en
---

On October 21, 2020, this year's meeting of members took place virtually. We thank all participants for their time, the constructive discussions and their readiness to take over a variety of tasks that are of great importance to our society. A new special interest group on "Computational Linguistics for the Political and Social Sciences" was presented and approved by the general meeting. In addition, the meeting decided that from 2021 on, the elections of the Executive and Advisory Boards will take place online. Also, from 2021 on, student memberships in the GSCL will be free. The KONVENS 2021 conference will take place in Düsseldorf (probably with parallel virtual participation options).