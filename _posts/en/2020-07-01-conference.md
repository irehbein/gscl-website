---
layout: post
title: SwissText & KONVENS Joint Conference 2020
feature-img: "assets/img/feature-img/konvens2020.png"
thumbnail: "assets/img/thumbnails/feature-img/konvens2020.png"
tags: [conferences]
lang: en
---

From June 23-25, 2020, the Joint SwissText and KONVENS Conference [link](https://swisstext-and-konvens-2020.org/), originally planned to be located in Zürich, took place online. SwissText has been organized by the Zürich University of Applied Sciences since 2016. KONVENS is an annual conference on Natural Language Processing usually taking place in Germany or Austria, sponsored by GSCL. The conference program featured the presentation of 38 papers or demos, four shared tasks as well as invited talks by Anya Belz, Roberto Navigli and Holger Schwenk. 132 participants from academia, government organizations and industry attended the virtual conference. The GSCL KONVENS 2020 Online Scholarship was awarded to Ian Clotworthy from the University of Potsdam.