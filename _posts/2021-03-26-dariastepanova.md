---
layout: post
title: Rule Induction and Reasoning over Knowledge Graphs
gscltalk: true
speaker: Daria Stepanova
organization: Bosch Center for Artificial Intelligence
speakerimage: "assets/img/people/talks/daria.jpg"
speakerimageteaser: "assets/img/people/talks/daria-300x225.jpg"
time: "16:00"
tags: [talks]
lang: de
---

Advances in information extraction have enabled the automatic construction of large knowledge graphs (KGs) like DBpedia, Freebase, YAGO and Wikidata. Learning rules from KGs is a crucial task for KG completion, cleaning and curation. This tutorial presents state-of-the-art rule induction methods, recent advances, research opportunities as well as open challenges along this avenue. We put a particular emphasis on the problems of learning exception-enriched rules from highly biased and incomplete data. Finally, we discuss possible extensions of classical rule induction techniques to account for unstructured resources (e.g., text) along with the structured ones.

# Biografie

Daria Stepanova is a research scientist at Bosch Center for Artificial Intelligence. Her research interests include Logic Programming, Description Logics, Inconsistency Management as well as Data Mining and Machine Learning. Previously Daria was a senior researcher at Max Plank Institute for Informatics (Germany), where she was heading a group on Semantic Data. Daria got her diploma degree in Applied Computer Science from the department of Mathematics and Mechanics of St. Petersburg State University (Russia) in 2010 and a PhD in Computational Logic from Vienna University of Technology (Austria) in 2015 under the supervision of Prof. Thomas Eiter. Before starting her PhD she worked as a visiting researcher at the school of Computing Science at Newcastle University (UK) in an industrially-oriented project and in a Russian company eKassir, which deals with the development of payment systems. 
