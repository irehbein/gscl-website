---
layout: post
title: SIG Text Technology
tags: [SIG]
lang: de
---

Der GSCL-Arbeitskreis "Texttechnologie" lädt ein zu Einreichungen für das 2022 erscheinende JLCL-Sonderheft "Computerlinguistische Herausforderungen, empirische Erforschung & multidisziplinäres Potenzial deutschsprachiger Songtexte". [Call for Papers](https://songkorpus.de/CfP_Songlyrics_JLCL_de.pdf)