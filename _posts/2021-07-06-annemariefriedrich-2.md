---
layout: post
title: "The INCEpTION Platform: Machine-Assisted and Knowledge-Oriented Interactive Annotation."
gscltalk: true
speaker: Annemarie Friedrich
organization: Bosch Center for Artificial Intelligence
speakerimage: "assets/img/inception.png"
speakerimageteaser: "assets/img/inception-300x225.png"
time: "13:30–15:00"
tags: [talks]
lang: de
---

Let the computer actively help you to enrich your texts with annotations and to link your texts to knowledge bases - this is what the INCEpTION text annotation platform helps you with.

No matter if you work alone or in a team or if you want to provide an annotation tool to your entire department - INCEpTION has you covered.

In this talk, you will learn about using the functionalities of INCEpTION including:

* importing texts for annotation
* basic text annotation
* linking annotations to a knowledge base
* customizing the annotations schema
* automatic annotation suggestions
* exporting the annotated texts

After the talk, you have the opportunity to use INCEpTION hands-on and to get answers to any questions you may have or run into.

NOTE: To participate in the hands-on part, it is recommended that you have already downloaded the latest version of INCEpTION from its homepage [1] and made sure that you can start in on your computer and that you can log in to it using a browser. Should you unexpectedly run into any problems trying to run INCEpTION on your computer before the event, best let us know [2] so we can help investigating the problem.

[1] Please install the Beta version: https://inception-project.github.io/downloads-beta/
[2] https://groups.google.com/d/forum/inception-users
[3] Please download the following sample files: https://drive.google.com/drive/folders/1RDJOL1shEHz95mvHY88KR4DnKhFlJYZZ
