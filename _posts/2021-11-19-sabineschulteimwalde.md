---
layout: post
title: Computational Models of Abstractness and Figurative Language
gscltalk: true
speaker: Sabine Schulte im Walde
organization: Universität Stuttgart
speakerimage: "assets/img/people/talks/sabine.jpg"
speakerimageteaser: "assets/img/people/talks/sabine-300x225.jpg"
time: "11:30"
tags: [talks]
lang: de
---

The distinction between abstract and concrete words (such as "dream" in contrast to "banana") is considered a highly relevant semantic categorisation for Natural Language Processing purposes. For example, previous studies have identified distributional abstractness as an important feature in automatic approaches to figurative language detection across languages (Turney et al. 2011, Tsvetkov et al. 2014, Köper & Schulte im Walde 2016, 2017, Aedmaa et al. 2018), cf. the literal vs. figurative meanings of "break the ice" in examples (1) vs. (2), with the concrete word "lake" in the literal meaning example (1) and the abstract word "conversation" in the figurative meaning example (2).

(1) [break the ice] on the lake <br />
(2) [break the ice] in the conversation

In this talk I will first present classifiers that successfully distinguish between literal and figurative language, relying heavily on distributional and abstractness/concreteness information. In the second part of the talk, I will zoom into concreteness norm properties, contextual characteristics of abstractness information as well as figurative language in discourse, in order to take an in-depth look into the interplay between abstractness and figurative language and to discuss the limits of this interaction.

# Biografie

Sabine Schulte im Walde is an Associate Professor at the Institute for Natural Language Processing at the University of Stuttgart. She studied Computational Linguistics and Cognitive Science at the Universities of Stuttgart and Edinburgh, and received a PhD in Computational Linguistics in 2003 from the University of Stuttgart, and the Venia Legendi (Habilitation) from Saarland University in 2009. From 2003-2004 she was a member of the Language Technology Group at the lexicographer DUDEN in Mannheim. In the past 10 years, Sabine has been the Principal Investigator of several research projects from the German Research Foundation (DFG), she was a Director of the Integrated Research Training Group for doctoral students in the DFG Collaborative Research Centre 732, and from 2011-2016 she was a DFG Heisenberg Fellow.

Sabine’s interdisciplinary research combines statistical NLP with linguistic and cognitive perspectives, to investigate computational and mathematical parameters in modelling human language and human judgements. Her main focuses in the past years have been on distributional representations and soft clustering for synchronic and diachronic ambiguity, compositionality, figurative language and lexical-semantic datasets and evaluation, with applications to terminology extraction and machine translation.

[schulteimwalde.de](http://schulteimwalde.de)
