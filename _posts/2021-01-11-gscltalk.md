---
layout: post
title: GSCL Tutorial Talk
tags: [talks]
lang: de
---

Diesen Mittwoch, am 13. Januar 2021, findet um 16 Uhr unser erster GSCL Tutorial Talk statt. Sprecher ist Prof. Dr. Casey Redd Kennington (Boise State University) zum Thema "Dialogue and Robots for Understanding Understanding". Mehr Informationen [hier](/events/talks). Die Login-Links zum virtuellen Event werden am Mittwoch per E-Mail auf der GSCL-Mitgliederliste versandt.