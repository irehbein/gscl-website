---
layout: post
title: GSCL Get2Know
feature-img: "assets/img/portfolio/ice-300x225.jpg"
img: "assets/img/portfolio/ice-1200x.jpg"
date: 11. September 2021
tags: [resources]
lang: de
---
# Ice Breaker Materials for GSCL Get2Know

### Vorbereitung

Wenn ihr möchtet, könnt ihr eure Lebensläufe etc. im Voraus teilen - aber das ist wirklich kein Muss, ihr könnt euch auch einfach ohne Vorbereitung treffen!

### Schritt 1: Wenn du ich wärst ...

Stellt euch abwechselnd kurz vor. Macht es jedoch nicht wie gewohnt, sondern wechselt die Perspektive. Wenn ich mich zum Beispiel vorstellen würde, würde ich sagen: „Wenn du Annemarie Friedrich wärst, würdest du als ältestes von vier Geschwistern in einem kleinen Dorf nahe dem Schwarzwald aufwachsen. Dein liebstes Hobby ist die Musik.. .. Später ziehst du ins Saarland, um Computerlinguistik zu studieren, weil ..." etc.

### Schritt 2: Fragen sammeln

Da die Zeit des Meetings wahrscheinlich begrenzt ist, stellt doch nun eine Liste mit Fragen oder Punkten zusammen, die ihr besprechen wollt.

### Schritt 3: Unterhalten :)

### Schritt 4: Feedback-Formular ausfüllen (Link per E-Mail gesendet)