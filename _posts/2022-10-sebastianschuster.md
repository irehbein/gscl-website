---
layout: post
title: October 2022 Research Talk
gscltalk: true
speaker: Sebastian Schuster
organization: NYU
speakerimage: "assets/img/people/talks/sebastianschuster.jpg"
speakerimageteaser: "assets/img/people/talks/sebastianschuster-300x225.jpg"
tags: [talks]
lang: de
---

Details to be announced.
