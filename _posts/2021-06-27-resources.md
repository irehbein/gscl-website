---
layout: post
title: Ethik Workshop
tags: [resources]
lang: de
---

Unter [Resources](/resources) sind ab sofort Materialien verfügbar, die in Folge des GSCL Ethik-Workshops 2020 erarbeitet wurden. Die Sammlung beinhaltet Lehrmaterial zum Thema Ethik in der Sprach- und Textverarbeitung sowie einen Fragebogen, der den zukünftigen Organisatoren von GermEval Shared Tasks eine Hilfestellung leisten soll.