---
layout: post
title: What about em?
gscltalk: true
speaker: Anne Lauscher
organization: Bocconi University, Milan
speakerimage: "assets/img/people/talks/annelauscher.jpg"
speakerimageteaser: "assets/img/people/talks/annelauscher-300x225.jpg"
tags: [talks]
time: "17:05 – 17:45"
lang: de
---

This is the second part of a two-part talk, in which Dr. Anne Lauscher will discuss their latest research centering around the current modeling of 3rd person pronouns in NLP as a concrete example.

# Biografie

Anne Lauscher is a postdoctoral researcher in the Natural Language Processing group at Bocconi University in Milan, Italy, where she is working on introducing demographic factors into language processing systems to improve algorithmic performance and system fairness. She obtained her Ph.D., awarded with the highest honors, from the Data and Web Science group at the University of Mannheim, Germany, where her research focused on the interplay between language representations and computational argumentation. During her studies, she also interned at and became an independent research contractor for Grammarly Inc. and the Allen Institute for Artificial Intelligence. Her research got published at top-tier international venues in Natural Language Processing and AI. Most recently, she received the Maria Gräfin von Linden-Award in life sciences/ STEM, which honors excellent female researchers in the German state of Baden-Württemberg. 