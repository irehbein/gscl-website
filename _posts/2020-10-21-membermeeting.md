---
layout: post
title: Mitgliederversammlung 2020
tags: [membermeeting]
lang: de
---

Am 21. Oktober 2020 fand die diesjährige Mitgliederversammlung virtuell statt. Wir danken allen Teilnehmern für ihre Zeit, die konstruktiven Diskussionen und ihre Bereitschaft, eine Vielzahl von Aufgaben zu übernehmen, die für unsere Gesellschaft von großer Bedeutung sind. Eine neue Arbeitsgruppe zum Thema "Computerlinguistik für die Politik- und Sozialwissenschaften" wurde vorgestellt und von der Mitgliederversammlung genehmigt. Darüber hinaus wurde auf der Sitzung beschlossen, dass die Wahlen des Exekutiv- und des Beirats ab 2021 online stattfinden. Ab 2021 sind außerdem studentische Mitgliedschaften in der GSCL kostenlos. Die Konferenz KONVENS 2021 findet in Düsseldorf statt (voraussichtlich mit parallelen virtuellen Beteiligungsmöglichkeiten).