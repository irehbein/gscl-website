---
layout: post
title: CPSS@Konvens 2021
feature-img: "assets/img/workshop2021.jpg"
thumbnail: "assets/img/workshop2021.jpg"
tags: [conferences]
lang: de
---

# 1st Workshop on Computational Linguistics for Political Text Analysis


Co-located with KONVENS 2021 in Düsseldorf, Germany (virtual event)

# Invited speakers

* Katharina Esau (Graduate School Online Participation, HHU Düsseldorf)
* Tomaž Erjavec (ParlaMint; Jožef Stefan Institute Ljubljana)
* Slava Jankin (Data Science Lab, Hertie School Berlin)

# Twin Panel Speakers

* EUINACTION (Nikoleta Yordanova, U-Leiden + Goran Glavaš, U-Mannheim)
* MARDY (Sebastian Haunss, U-Bremen + Jonas Kuhn, U-Stuttgart)


# Workshop description

Recent years have seen more and more applications of computational methods to the analysis of political texts. This has led to the emergence of different communities (one with a background in political / social science and the other in the areas of NLP/computer science) which struggle to get awareness of the relevant work taking place in the respective other community.

The main goal of our workshop is to bridge this gap and bring together researchers and ideas from the different communities, to foster collaboration and catalyze further interdisciplinary research efforts.

To this end, our workshop will accept two types of workshop submissions:

+ archival papers describing original and unpublished work
* non-archival papers (abstracts) that present already published research or ongoing work.

The availability of both submission formats will meet the different needs of researchers from different communities, allowing them to come together and exchange ideas in an "get to know each other" environment which is likely to foster future collaborations.

In addition, the workshop will host a panel with invited speakers from the different communities, with a focus on interdisciplinary methods and strategies supporting research at the intersection of Natural Language Processing and Political/Social science. In particular, we are interested in learning from previous experiences of interdisciplinary projects to gain a better understanding of what has made collaborations at the NLP/SocSci interface successful (e.g., strategies to optimize the conceptual/practical exchange between teams) as well as of the concrete problems encountered and about their solutions. Therefore, we will invite "twin researchers" to the panel, i.e., pairs of researchers where each pair includes one researcher from political or social science and one with a background in CL/CS, who have worked together on a project. The twin researchers will talk about their collaboration and share their experiences with the audience.

We hope that this will foster discussions and allow us to reflect on our different research practices, methods and tools, and will help to improve the communication between our fields.

# Organisers

* Ines Rehbein, Goran Glavaš, Simone Ponzetto (U-Mannheim)
* Gabriella Lapesa (U-Stuttgart)

# Workshop Programme

[go to Programme](https://gscl.org/en/arbeitskreise/cpss/cpss-2021/workshop-programme)

[Workshop Proceedings](https://gscl.org/en/arbeitskreise/cpss/cpss-2021/workshop-proceedings)

go to Proceedings

Programme committee

[go to PC](https://gscl.org/en/arbeitskreise/cpss/cpss-2021/pc)

Call for papers

[go to CfP](https://gscl.org/en/arbeitskreise/cpss/cpss-2021/cfp)

