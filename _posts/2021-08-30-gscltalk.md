---
layout: post
title: GSCL Talks
tags: [talks]
lang: de
---

Die GSCL Talks sind zurück aus der Sommerpause. Wir starten am 16. September 2021 um 16 Uhr mit einem Vortrag von Prof. Dr. Alexander Koller (Universität des Saarlandes) zum Thema "Compositional Semantic Parsing" ([Details hier](https://gscl.org/de/events/talks/september-2021-research-talk)).