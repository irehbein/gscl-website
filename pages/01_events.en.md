---
layout: page
title: Events
permalink: /events/
feature-img: "assets/img/events.jpg"
lang: en
tags: [Page]
---


The GSCL Talks are an initiative starting in 2021 with the aim of bringing our community closer together via regular virtual technical and linguistic discussions. There are two formats: Research Talks which feature invited talks on recent research topics, and Tutorial Talks intended for students and doctoral students. Participation links will be sent out to members shortly before each meeting. [How to become a member]

Any questions? Contact us!
Organizer: Annemarie Friedrich, annemarie.friedrich@gmail.com

## GSCL Research Talks

The GSCL Research Talks, which will take place from February 2021 on, offer a forum for exchange about recent research topics for academia and industry. Speakers include senior-level researchers from academia and industry. Talks will be held in German or English with each talk's language announced beforehand. The duration of the talk is one hour including 30 minutes Q&A.

## GSCL Tutorial Talks

GSCL Tutorial Talks include 90-minute sessions on specific topics within computational linguistics that are not usually within the curriculum, deep dives into a particular research project, or introductory lectures to neighboring disciplines. The series also features interviews with experts on their personal experiences and presentations of industrial research. It is a forum for students and doctoral students to connect with experts, learn about internship or hiring opportunities and network with experts and fellow students from other universities.

{% include gscltalkslast.html %}

[Archive of all GSCL talks](gscltalksarchive/)


# KONVENS

* [KONVENS 2021 in Düsseldorf](https://konvens2021.phil.hhu.de/)  
* [KONVENS 2020 in Zürich](https://swisstext-and-konvens-2020.org/)  
* [KONVENS 2019 in Erlangen](https://2019.konvens.org/)  
* [KONVENS 2018 in Wien](https://www.oeaw.ac.at/ac/konvens2018/)  
* [KONVENS 2016 in Bochum](https://www.linguistics.rub.de/konvens16/)  
* [KONVENS 2014 in Hildesheim](http://www.uni-hildesheim.de/konvens2014/)  
* [KONVENS 2012 in Wien](http://www.oegai.at/konvens2012/)  
* [KONVENS 2010 in Saarbrücken](http://konvens2010.coli.uni-saarland.de/)  
* [KONVENS 2008 in Berlin](http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=2482&copyownerid=706)  
* [KONVENS 2006 in Konstanz](http://ling.uni-konstanz.de/pages/conferences/konvens06/)  
* KONVENS 2004 in Wien  
* [KONVENS 2002 in Saarbrücken](http://konvens2002.dfki.de/)  
* [KONVENS 2000 in Ilmenau](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens2000.html)  
* [KONVENS 1998 in Bonn](http://www.ikp.uni-bonn.de/forschung/konferenzen-workshops-und-symposia/konvens-98/konvens-98/)  
* [KONVENS 1996 in Bielefeld](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens1996.html)  
* [KONVENS 1994 in Wien](http://www.oegai.at/konvens94.shtml)  
* [KONVENS 1992 in Nüremberg](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens1992.html)


## GSCL/GLDV-Conferences (every two years until 2017, since then as KONVENS)
* [GSCL Conference 2017 in Berlin](http://gscl2017.dfki.de/)  
* [GSCL Conference 2015 in Duisburg/Essen](http://gscl2015.inf.uni-due.de/)  
* [GSCL Conference 2013 in Darmstadt](http://gscl2013.ukp.informatik.tu-darmstadt.de/)  
* [GSCL Conference 2011 in Hamburg](http://exmaralda.org/gscl2011/)  
* [GSCL Conference 2009 in Potsdam](http://www.ling.uni-potsdam.de/acl-lab/gscl09/)  
* [GLDV Conference 2007 in Tübingen](http://www.sfb441.uni-tuebingen.de/gldv2007/)  
* GLDV Conference 2005 in Bonn  
* [GLDV Conference 2003 in Köthen](http://www.informatik.uni-trier.de/~ley/db/journals/ldvf/ldvf18.html)  
* [GSCL Conference 2001 in Gießen](http://www.uni-giessen.de/germanistik/ascl/gldv2001/)  
* [GLDV Conference 1999 in Frankfurt](http://titus.uni-frankfurt.de/curric/gldv99.htm)  
* [GLDV Conference 1997 in Leipzig](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1997.html)  
* [GLDV Conference 1995 in Regensburg](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1995.html)  
* [GLDV Conference 1993 in Kiel](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1993.html)  
* GLDV Conference 1991 in Trier  
* [GLDV Conference 1990 in Siegen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1990.html)  
* [GLDV Conference 1989 in Ulm](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1989.html)  
* GLDV Conference 1988 in Saarbrücken  
* [GLDV Conference 1987 in Bonn](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1987.html)  
* [GLDV Conference 1986 in Göttingen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1986.html)  
* GLDV Conference 1985 in Hannover  
* GLDV Conference 1984 in Heidelberg  
* [GLDV Conference 1983 in Trier](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1983.html)  
* GLDV Conference 1982 in Koblenz  
* GLDV Conference 1980 in Saarbrücken  
* GLDV Conference 1979 in Bonn  
* GLDV Conference 1978 in Essen  
* GLDV Conference 1976 in München

# TaCoS

TaCoS is a conference series dating back to the 90s that is open to any student interested in the areas of computational linguistics and natural language processing as well as its neighboring applied and theoretical disciplines from psycholinguistics to computer science.