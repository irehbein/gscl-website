---
layout: page
title: Veranstaltungen
permalink: /events/
feature-img: "assets/img/events.jpg"
lang: de
tags: [Page]
---

Die von der GSCL (mit-)organisierten Fachtagungen und virtuellen Veranstaltungen befördern den Erfahrungsaustausch auf hohem Niveau durch die Vorstellung computerlinguistischer Grundlagenforschung und ausgewählte Praxisvorträge von Experten. Sie dienen als Plattform für die Präsentation herausragender Nachwuchsarbeiten sowie für die Verleihung der GSCL-Preise.

# GSCL Research and Tutorial Talks

Mit dem Ziel eines regelmäßigen technischen und linguistischen Austauschs finden ab Januar 2021 regelmäßig virtuelle Veranstaltungen statt. Es sind zwei Formate geplant: "Research Talks" in Form von eingeladenen Vorträgen zu aktuellen Forschungsthemen, und "Tutorial Talks" mit der primären Zielgruppe Studierende und Doktoranden. Teilnahmelinks werden jeweil kurz vor den Veranstaltungen an alle GSCL-Mitglieder versandt.

Fragen?
Organisatoren: Annemarie Friedrich, annemarie.friedrich@gmail.com

#### GSCL Research Talks

Die GSCL Research Talks, die ab Februar 2021 stattfinden, bieten ein Forum für den Austausch über aktuelle Forschungsthemen in der Wissenschaft und der Industrie. Zu den Vortragenden zählen hochrangige Forscher aus Wissenschaft und Industrie. Die Vorträge werden auf Deutsch oder Englisch gehalten, wobei die Sprache jedes Vortrags im Voraus bekannt gegeben wird. Die Veranstaltungsdauer beträgt eine Stunde, einschließlich 30 Minuten Fragen und Antworten.

#### GSCL Tutorial Talks

Die 90minütigen GSCL Tutorial Talks behandeln spezielle Themen der Computerlinguistik, die normalerweise nicht im Lehrplan enthalten sind, Einblicke in Forschungsprojekte oder Einführungsvorträge in benachbarte Disziplinen. Die Reihe enthält auch Interviews mit Experten über ihre persönlichen Erfahrungen und Präsentationen aus der industriellen Forschung. Die GSCL Tutorial Talks sind ein Forum für Studierende und Doktoranden, um sich mit Experten auszutauschen, sich über Praktikums- oder Einstellungsmöglichkeiten zu informieren und sich mit Experten und Kommilitonen anderer Universitäten zu vernetzen.

{% include gscltalkslast.html %}

[Archiv aller GSCL Talks](gscltalksarchive/)

Fragen?
Organisatoren: Annemarie Friedrich, annemarie.friedrich@gmail.com

# KONVENS

* [KONVENS 2021 in Düsseldorf](https://konvens2021.phil.hhu.de/)  
* [KONVENS 2020 in Zürich](https://swisstext-and-konvens-2020.org/)  
* [KONVENS 2019 in Erlangen](https://2019.konvens.org/)  
* [KONVENS 2018 in Wien](https://www.oeaw.ac.at/ac/konvens2018/)  
* [KONVENS 2016 in Bochum](https://www.linguistics.rub.de/konvens16/)  
* [KONVENS 2014 in Hildesheim](http://www.uni-hildesheim.de/konvens2014/)  
* [KONVENS 2012 in Wien](http://www.oegai.at/konvens2012/)  
* [KONVENS 2010 in Saarbrücken](http://konvens2010.coli.uni-saarland.de/)  
* [KONVENS 2008 in Berlin](http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=2482&copyownerid=706)  
* [KONVENS 2006 in Konstanz](http://ling.uni-konstanz.de/pages/conferences/konvens06/)  
* KONVENS 2004 in Wien  
* [KONVENS 2002 in Saarbrücken](http://konvens2002.dfki.de/)  
* [KONVENS 2000 in Ilmenau](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens2000.html)  
* [KONVENS 1998 in Bonn](http://www.ikp.uni-bonn.de/forschung/konferenzen-workshops-und-symposia/konvens-98/konvens-98/)  
* [KONVENS 1996 in Bielefeld](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens1996.html)  
* [KONVENS 1994 in Wien](http://www.oegai.at/konvens94.shtml)  
* [KONVENS 1992 in Nüremberg](http://www.informatik.uni-trier.de/~ley/db/conf/konvens/konvens1992.html)

# GSCL/GLDV-Conferences

Die GSCL/GLDV-Konferenzen fanden bis 2017 zweijährlich statt, seitdem als KONVENS.

* [GSCL Conference 2017 in Berlin](http://gscl2017.dfki.de/)  
* [GSCL Conference 2015 in Duisburg/Essen](http://gscl2015.inf.uni-due.de/)  
* [GSCL Conference 2013 in Darmstadt](http://gscl2013.ukp.informatik.tu-darmstadt.de/)  
* [GSCL Conference 2011 in Hamburg](http://exmaralda.org/gscl2011/)  
* [GSCL Conference 2009 in Potsdam](http://www.ling.uni-potsdam.de/acl-lab/gscl09/)  
* [GLDV Conference 2007 in Tübingen](http://www.sfb441.uni-tuebingen.de/gldv2007/)  
* GLDV Conference 2005 in Bonn  
* [GLDV Conference 2003 in Köthen](http://www.informatik.uni-trier.de/~ley/db/journals/ldvf/ldvf18.html)  
* [GSCL Conference 2001 in Gießen](http://www.uni-giessen.de/germanistik/ascl/gldv2001/)  
* [GLDV Conference 1999 in Frankfurt](http://titus.uni-frankfurt.de/curric/gldv99.htm)  
* [GLDV Conference 1997 in Leipzig](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1997.html)  
* [GLDV Conference 1995 in Regensburg](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1995.html)  
* [GLDV Conference 1993 in Kiel](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1993.html)  
* GLDV Conference 1991 in Trier  
* [GLDV Conference 1990 in Siegen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1990.html)  
* [GLDV Conference 1989 in Ulm](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1989.html)  
* GLDV Conference 1988 in Saarbrücken  
* [GLDV Conference 1987 in Bonn](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1987.html)  
* [GLDV Conference 1986 in Göttingen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1986.html)  
* GLDV Conference 1985 in Hannover  
* GLDV Conference 1984 in Heidelberg  
* [GLDV Conference 1983 in Trier](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1983.html)  
* GLDV Conference 1982 in Koblenz  
* GLDV Conference 1980 in Saarbrücken  
* GLDV Conference 1979 in Bonn  
* GLDV Conference 1978 in Essen  
* GLDV Conference 1976 in München

# TaCoS

[TaCoS](https://linguistik.computer) ist eine Konferenzreihe, die bereits in den 90er Jahren etabliert wurde und allen Studenten offen steht, die sich für die Bereiche Computerlinguistik und Verarbeitung natürlicher Sprache sowie deren angrenzender angewandter und theoretischer Disziplinen von Psycholinguistik bis Informatik interessieren.
