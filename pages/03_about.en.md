---
layout: page
title: About the GSCL
permalink: /about/
feature-img: "assets/img/aboutus.jpg"
tags: [Page]
lang: en
---




The German Society for Computational Linguistics and Language Technology (GSCL) is the scientific association in the German-speaking countries and regions for research, teaching and professional work in natural language processing.

# Executive Board of the GSCL
The German Society for Language Technology and Computational Linguistics acts through its honorary mandate holders. The executive committee is responsible for all matters of the GSCL - e.g. convocation of the general meeting and implementation of its decisions, preparation of a budget, representation of GSCL to the outside - unless they are assigned by the statute to another body.

**Prof. Dr. Torsten Zesch**, President

![]({{ "/assets/img/people/zesch.jpg" | relative_url }})

[Email](mailto:vorsitzende@gscl.org) [Homepage](https://www.ltl.uni-due.de/team/torsten-zesch/)



**Dr. Annemarie Friedrich**, Vice President

![]({{ "/assets/img/people/friedrich-1408x.jpg" | relative_url }})

[Email](mailto:vorsitzende@gscl.org) [Homepage](https://sites.google.com/view/annemariefriedrich/home)


**Dr. Josef Ruppenhofer**, Information Officer

[Email](mailto:informationsreferent@gscl.org) [Homepage](https://www.ids-mannheim.de/en/prag/personal/ruppenhofer/)



**PhD Gertrud Faaß**, Treasurer

![]({{ "/assets/img/people/faass-1408x.jpg" | relative_url }})

[Email](mmailto:schatzmeister@gscl.org) [Homepage](https://www.uni-hildesheim.de/fb3/institute/iwist/mitglieder/faass/)



**Dr. Bernhard Fisseni**, Secretary

![]({{ "/assets/img/people/fisseni-1408x.jpg" | relative_url }})

[Email](mmailto:schrift@gscl.org) [Homepage](http://www.uni-due.de/~hg0132/)



**Prof. Dr. Christian Wartena**, Editor of the JLCL

[Email](mmailto:jlcl@gscl.orgg) [Homepage](https://im.f3.hs-hannover.de/en_us/studium/personen/prof-dr-christian-wartena/)



### GSCL Advisory Board

The Advisory Board has the task of advising and assisting the Board in all matters relating to the work and tasks of the Association, including budgetary matters.



**PD Dr. Roman Schneider**, Sprecher, Leibniz-Institut für Deutsche Sprache 

![]({{ "/assets/img/people/schneider.jpg" | relative_url }})


**Prof. Dr. Cerstin Mahlow**, Zürcher Hochschule für Angewandte Wissenschaften (ZHAW)

![]({{ "/assets/img/people/mahlow.jpg" | relative_url }})

**Dr. Ines Rehbein**, Universität Mannheim 

![]({{ "/assets/img/people/rehbein.jpg" | relative_url }})


**Prof. Dr. Bernhard Schroeder**, Universität Duisburg-Essen

![]({{ "/assets/img/people/schroeder.jpg" | relative_url }})


**Prof. Dr. Heike Zinsmeister**, Universität Hamburg

![]({{ "/assets/img/people/zinsmeister.jpg" | relative_url }})

**DR. Nicolai Erbs**, DB Fernverkehr

![]({{ "/assets/img/people/erbs-1408x.jpg" | relative_url }})


# [Über uns](#ueber-uns)

Mission and Charter

The GSCL is the scientific association for research, teaching and professional work on natural language processing. It registered as a non-profit association, the charter can be found [here]({{ "/assets/satzung.pdf" | relative_url }}). The society represents the interests of its members and promotes cooperation between the members and their working fields, such as:

Die GSCL ist der wissenschaftliche Fachverband für maschinelle Sprachverarbeitung in Forschung, Lehre und Beruf. Sie ist ein gemeinnütziger eingetragener Verein ([Satzung]({{ "/assets/satzung.pdf" | relative_url }})) und vertritt die Interessen ihrer Mitglieder nach außen und fördert die Kooperation zwischen den Mitgliedern und ihren verschiedenen Arbeitsbereichen. Zu diesen Arbeitsbereichen gehören:

* automatic NL analysis and generation
* corpus engineering and corpus linguistics
* document processing and text technology
* information retrieval and knowledge management
* artificial intelligence and machine learning
* dictionaries and terminological databases
* machine translation
* human-machine communication
* multimedia and hypermedia
* tools for literary and linguistic research


The GSCL also supports [shared task initiatives](http://www.computerlinguistik.org/portal/portal.html?s=Shared%20Tasks) like [GermEval](https://germeval.github.io/), the cooperation with neighbouring disciplines (e.g., linguistics and semiotics, computer science and mathematics, psychology and cognitive science, data and information science, digital humanities), and keeps contact to the respective associations. There are international contacts with organisations like the [Association for Computational Linguistics](https://www.aclweb.org/portal/) and the [European Association for Digital Humanities](https://eadh.org/) (EADH), formerly Association for Literary and Linguistic Computing (ALLC).



 {% include about.html %}
