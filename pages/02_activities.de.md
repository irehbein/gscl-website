---
layout: page
title: Aktivitäten
permalink: /activities/
subtitle: von der GSCL
feature-img: "assets/img/computer.jpg"
lang: de
tags: [Page]
toc: true
---

# Arbeitskreise
Die Arbeitskreise der GSCL dienen als Kommunikations- und Arbeitsplattform für Interessierte aus Wissenschaft und Industrie und sollen Erfahrungsaustausch und Wissenstransfer innerhalb des jeweiligen Forschungsnetzwerks befördern.
Arbeitskreise befassen sich gezielt mit einzelnen Schwerpunkten aus dem Themenspektrum der Computerlinguistik und angrenzender Forschungsgebiete.
In diesem Zusammenhang werden Arbeitstreffen, Vorträge und Workshops organisiert sowie gemeinsame Publikationen erarbeitet.
Und, nicht zuletzt: Viele Studierende haben in diesem Rahmen erstmals "ihr" Projekt einem größeren Fachpublikum vorgestellt und damit ihre ersten Schritte auf dem wissenschaftlichen Podium getan.
Die Mitgliedschaft in den Arbeitskreisen ist für GSCL-Mitglieder gebührenfrei.
- [Computerlinguistik für die Politik- und Sozialwissenschaften](politicssocialsciences)
- [Texttechnologie](texttechnology)
- [Korpuslinguistik](corpuslinguistics)
- [Stimmungsanalyse](germansentiment)
- [Social Media / Internetbasierte Kommunikation](socialmedia)
- [Ethik](/resources/ethics-crash-course)


# Publikationen

## Konferenz-Proceedings

Proceedings der KONVENS und der GSCL-Konferenzen werden gesammelt unter einer [einheitlichen Adresse](https://konvens.org/site/proceedings) zur Verfügung gestellt. Ab dem Jahr 2021 werden die KONVENS Proceedings in der [ACL Anthology](https://aclanthology.org/venues/konvens/) archiviert.

## Journal for Language Technology and Computational Linguistics (JLCL)

Publikationsorgan der GSCL ist das [Journal for Language Technology and Computational Linguistics (JLCL)](http://www.jlcl.org/), ehemals LDV-Forum. Die Zeitschrift veröffentlicht Fachbeiträge und Berichte, Diskussionen und Rezensionen aus dem gesamten Spektrum der Sprachtechnologie und Computerlinguistik; einzelne Hefte erscheinen dabei zu besonderen Themenschwerpunkten. Über die Annahme von Fachbeiträgen entscheiden die Herausgeber nach (mindestens zwei) Gutachten von Mitgliedern des wissenschaftlichen Beirats (Herausgebergremium).



## Portal Computerlinguistik

Das [Portal Computerlinguistik](http://www.computerlinguistik.org/) bietet Verweise auf CL-relevante Institutionen, Projekte, Ressourcen, Ausbildungsmöglichkeiten, Stellenanzeigen und Veranstaltungen.

![Portal Computerlinguistik]({{ "/assets/img/cl-portal.jpg" | relative_url }}) "Portal Computerlinguistik")



## GSCL-Mailingliste

Beiträge mit Bezug zur Computerlinguistik, Fachinformationen, Kurzrezensionen von Büchern und Software, Veranstaltungsinformationen, Anfragen, Diskussionsanregungen etc. können jederzeit per E-Mail an die GSCL-Mailingliste eingereicht werden. Darüber hinaus nutzt die GSCL [Twitter](https://twitter.com/gscl_org) and [LinkedIn](https://www.linkedin.com/company/german-society-for-computational-linguistics-and-language-technology-gscl/) für aktuelle Kurzmeldungen. GSCL-Mitglieder erhalten regelmäßig den GSCL-Newsletter in gedruckter Form.



## Bücher

Bücher der Reihe Sprache und Computer (Hrsg.: P. Hellwig, J. Krause) im [Georg Olms Verlag Hildesheim](http://www.olms.de/) können GSCL-Mitglieder preisermäßigt beziehen.

# Auszeichnungen

Die GSCL vergibt Auszeichnungen für studentische Abschlussarbeiten (BA/MA) und Dissertationen im jährlichen Wechsel.
- [GSCL-Promotionspreis zum Gedenken an Wolfgang Hoeppner](phdaward/)
- [GSCL-Preis für studentische Abschlussarbeiten](studentaward/)

