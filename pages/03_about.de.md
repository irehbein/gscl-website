---
layout: page
title: Über die GSCL
permalink: /about/
feature-img: "assets/img/aboutus.jpg"
lang: de
tags: [Page]
---

Die Gesellschaft für Sprachtechnologie und Computerlinguistik e.V. (GSCL) ist der wissenschaftliche Fachverband in den deutschsprachigen Ländern und Regionen für maschinelle Sprachverarbeitung in Forschung, Lehre und Beruf. Sie ist aktiv um Verbindungen zwischen Hochschulen und Industrie bemüht und unterstützt die Zusammenarbeit mit Nachbardisziplinen wie z.B. Linguistik und Semiotik, Informatik und Mathematik, Psychologie und Kognitionswissenschaft, Informations- und Dokumentationswissenschaft und unterhält Kontakte zu den entsprechenden Fachverbänden.


# Über uns

Die GSCL ist der wissenschaftliche Fachverband für maschinelle Sprachverarbeitung in Forschung, Lehre und Beruf. Sie ist ein gemeinnütziger eingetragener Verein ([Satzung]({{ "/assets/satzung.pdf" | relative_url }})) und vertritt die Interessen ihrer Mitglieder nach außen und fördert die Kooperation zwischen den Mitgliedern und ihren verschiedenen Arbeitsbereichen. Zu diesen Arbeitsbereichen gehören:

*   Automatische Analyse und Generierung natürlicher Sprache
*   Corpus Engineering und Korpuslinguistik
*   Dokumentenverarbeitung und Texttechnologie
*   Information Retrieval und Wissensmanagement
*   Multimedia und Hypermedia
*   Lexika und Terminologiedatenbanken
*   Maschinelle Übersetzung
*   Mensch-Maschine-Kommunikation
*   Künstliche Intelligenz und Maschinelles Lernen
*   Werkzeuge für die Philologie und Sprachwissenschaft

Dabei gibt es ein Kontinuum zwischen Forschung und praktischer Anwendung. Die GSCL ist aktiv um Verbindungen zwischen Hochschulen und Industrie bemüht. Herausragende studentische Abschlussarbeiten werden regelmäßig mit dem GSCL-Preis prämiert.

Die GSCL unterstützt [Shared Task-Initiativen](http://www.computerlinguistik.org/portal/portal.html?s=Shared%20Tasks) wie [GermEval](https://germeval.github.io/), die Zusammenarbeit mit Nachbardisziplinen (wie z.B. Linguistik und Semiotik, Informatik und Mathematik, Psychologie und Kognitionswissenschaft, Informations- und Dokumentationswissenschaft, Digital Humanities), und unterhält Kontakte zu entsprechenden Fachverbänden. International kooperiert sie mit Organisationen wie der [Association for Computational Linguistics](https://www.aclweb.org/portal/) (ACL) und der [European Association for Digital Humanities](https://eadh.org/) (EADH), vormals Association for Literary and Linguistic Computing (ALLC).

# Vorstand
Die Leitung der GSCL erfolgt durch ehrenamtliche Mandatsträger. Der Vorstand kümmert sich um alle Belange inklusive der Einberufung der Mitgliederversammlung, der Umsetzung von Entscheidungen, der Haushaltsplanung und der externen Repräsentation der GSCL, soweit die Satzung diese Aufgaben nicht anderen Funktionen zuweist.


 {% include board_de.html %}



# Beirat

Der Beirat berät den Vorstand in allen Belangen die Arbeit und Aufgaben der GSCL betreffend inklusive der Haushaltsplanung.

{% include boardadvisors_de.html %}

# Weitere Informationen


 {% include about.html %}
