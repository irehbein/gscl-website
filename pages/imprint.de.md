---
layout: page
title: Impressum & Datenschutz
permalink: /imprint
lang: de
tags: [Page]
hide: true
footer: true
---

**Gesellschaft für Sprachtechnologie und Computerlinguistik e.V.
(GSCL)**\
E-Mail: GSCL-Vorstand\@gscl.org

Vertretungsberechtigt:

**Erster Vorsitzender:**\
Prof. Dr. Torsten Zesch\
Universität Duisburg-Essen\
Forsthausweg (Gebäude LE)\
47057 Duisburg\
[Homepage]

**Registergericht:** Amtsgericht Hildesheim\
**Registernummer:** VR8710

**V.i.S.d § 55 Abs. 2 RStV:**\
Prof. Dr. Torsten Zesch\
Universität Duisburg-Essen\
Forsthausweg (Gebäude LE)\
47057 Duisburg

**Datenschutz**

Die Nutzung unserer Website ist in der Regel ohne Angabe
personenbezogener Daten möglich. Protokolliert werden Daten über
Zugriffe auf die Website, also z.B. Datum und Uhrzeit, Menge der
gesendeten Daten in Byte, verwendeter Browser, verwendete IP-Adresse.
Diese Daten werden auf unserer Website als \"Server-Logfiles\"
gespeichert und regelmäßig gelöscht. Zur Erhöhung der
Benutzerfreundlichkeit und Sicherheit dieser Website kommen Cookies zum
Einsatz. Sie können in den Einstellungsoptionen Ihres Browser bei Bedarf
die Verwendung dieser Cookies deaktivieren. Soweit auf unseren Seiten
personenbezogene Daten (beispielsweise Name, Anschrift oder
E-Mail-Adresse) erhoben werden, erfolgt dies stets auf freiwilliger
Basis, zur Mitgliederverwaltung und zur Duchführung der Vereinsaufgaben.
Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte
weitergegeben. Der Nutzung von im Rahmen der Impressumspflicht
veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht
ausdrücklich angeforderter Werbung und Informationsmaterialien wird
hiermit ausdrücklich widersprochen.

# [Images]

Bild \"Alte Bücher\" von [Michal Jarmoluk] auf [Pixabay]

Bild \"Leipzig Stadt Aussicht\" von [Thomas Wolter] auf [Pixabay][2]

Bild \"Video Conference\" von [febrian eka saputra] auf
:::

  [1]: #impressum-datenschutz {.color-inherit .text-decoration-0}
  [Homepage]: http://www.ltl.uni-due.de/team/torsten-zesch/
  [Images]: #images {.color-inherit .text-decoration-0}
  [Michal Jarmoluk]: https://pixabay.com/de/users/jarmoluk-143740/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Thomas Wolter]: https://pixabay.com/de/users/thomaswolter-92511/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [2]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [febrian eka saputra]: https://pixabay.com/de/users/febrianes86-5873902/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5484678

Bild von [Deedster] auf [Pixabay]

  [Deedster]: https://pixabay.com/de/users/deedster-2541644/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  
  Bild von [Here and now, unfortunately, ends my journey on Pixabay] auf
[Pixabay]

  [Here and now, unfortunately, ends my journey on Pixabay]: https://pixabay.com/de/users/alexas_fotos-686414/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300

  Bild von [Наталья Коллегова] auf [Pixabay]
  

  [Наталья Коллегова]: https://pixabay.com/de/users/natalia_kollegova-5226803/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913