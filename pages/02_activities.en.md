---
layout: page
title: Activities
permalink: /activities/
subtitle: powered by GSCL
feature-img: "assets/img/computer.jpg"
lang: en
tags: [Page]
toc: true
---

# Special Interest groups

The special interest groups of GSCL serve as a platform for exchange and collaboration for interested parties from science and industry and are intended to promote the exchange of experience and knowledge transfer within the respective research network.

Special interest groups focus on specific topics from the field of computational linguistics and related research areas. Their activities include the organization of meetings, lectures and workshops as well as joint publications.

And, last but not least: Many students have presented "their" project to a larger professional audience for the first time in the setting of a SIG, thereby dipping their toes in the waters of science.

Membership in the working groups is free of charge for GSCL members.
- [Computational Linguistics for the Political and Social Sciences](politicssocialsciences)
- [Text Technology](texttechnology)
- [Corpus Linguistics](corpuslinguistics)
- [Sentiment Analysis](germansentiment)
- [Social Media / Internet-based Communication](socialmedia)
- [Ethics](/en/resources/ethics-crash-course)

# Publications

## Conference Proceedings

The Proceedings of KONVENS and GSCL conferences will be made available collectively at a [unified address](https://konvens.org/site/proceedings). Beginning with 2021's edition, the proceedings of KONVENS have been archived in the [ACL Anthology](https://aclanthology.org/venues/konvens/).

## Journal for Language Technology and Computational Linguistics (JLCL).

The publication outlet of GSCL is the [Journal for Language Technology and Computational Linguistics (JLCL)](http://www.jlcl.org/), formerly LDV Forum. The journal publishes technical papers and reports, discussions and reviews from the entire spectrum of language technology and computational linguistics. JLCL also regularly features issues dedicated to special topics and edited by guest editors. The editors decide on the acceptance of technical contributions after (at least two) reviews by members of the scientific advisory board (editorial board).


## Portal Computational Linguistics

The web portal [Computational Linguistics](http://www.computerlinguistik.org/) provides references to CL-related institutions, projects, resources, educational opportunities, job postings, and events.

![Portal Computational Linguistics]({{ "/assets/img/cl-portal.jpg" | relative_url }}) "Portal Computational Linguistics")


## GSCL mailing list

Postings related to computational linguistics, technical information, short reviews of books and software, announcements of conferences and events, inquiries, discussion suggestions, etc. can be submitted to the GSCL mailing list at any time by e-mail. In addition, GSCL uses [Twitter](https://twitter.com/gscl_org) and [LinkedIn](https://www.linkedin.com/company/german-society-for-computational-linguistics-and-language-technology-gscl/) for news items on ongoing events. GSCL members receive regular hard copies of the GSCL newsletter.


## Books

Books in the series "Sprache und Computer" [Language and Computers] (eds.: P. Hellwig, J. Krause) published by [Georg Olms Verlag Hildesheim](http://www.olms.de/) are available to GSCL members at a reduced price.

# Awards

The GSCL awards prizes for student theses (BA/MA) and dissertations on an annual basis.
- [GSCL doctoral dissertation award in memory of Wolfgang Hoeppner](phdaward/)
- [GSCL award for student theses](studentaward/)
